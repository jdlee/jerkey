package com.steeplesoft.jerkey.server.impl

import com.steeplesoft.jerkey.server.Application
import com.steeplesoft.jerkey.server.CallContext
import org.glassfish.jersey.jackson.JacksonFeature
import org.glassfish.jersey.media.sse.SseFeature
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.model.Resource

class JerkeyResourceConfig(val application: Application) : ResourceConfig() {
    init {
        register(JacksonFeature::class.java)
        val resourceBuilder = Resource.builder()

        resourceBuilder.path(application.path)
        application.resources.forEach { res ->
            println("Configuring ${res.method} for ${res.path}")
            val childResource = resourceBuilder.addChildResource(res.path)
            val methodBuilder =
                    childResource.addMethod(res.method.toUpperCase())
                            .encodedParameters(true)
                            .extended(true)
                            .produces(res.produces)
                            .consumes(res.consumes)
                            .handledBy(CallContext(res))
            if (res.sse) {
                methodBuilder.sse()
                        .produces(SseFeature.SERVER_SENT_EVENTS)
            }
        }
        registerResources(resourceBuilder.build())
    }
}
