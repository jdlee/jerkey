package com.steeplesoft.jerkey.server

import kotlin.reflect.KClass

data class Param(var source: String = "query",
    var name: String? = null,
    var type: KClass<*>? = null)