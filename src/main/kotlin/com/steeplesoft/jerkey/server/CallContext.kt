package com.steeplesoft.jerkey.server

import org.glassfish.jersey.process.Inflector
import org.glassfish.jersey.server.ContainerRequest
import java.time.Instant
import java.time.format.DateTimeFormatter
import javax.ws.rs.WebApplicationException
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.core.*
import javax.ws.rs.ext.ParamConverterProvider

class CallContext(private val res: com.steeplesoft.jerkey.server.Resource) :
        Inflector<ContainerRequestContext, Response> {
    @Context
    var httpHeaders: HttpHeaders? = null
    @Context
    var securityContext: SecurityContext? = null
    @Context
    var uriInfo: UriInfo? = null
    @Context
    var paramConverterProvider: ParamConverterProvider? = null

    var params = mutableMapOf<String, Any>()
    var body: Any? = null

    override fun apply(crc: ContainerRequestContext): Response {
        try {
            res.bodyType?.let {
                body = (crc as ContainerRequest).readEntity(it.java)
            }
            (crc.uriInfo.queryParameters + crc.uriInfo.pathParameters).forEach { qp ->
                val name = qp.key!!
                val value = qp.value[0]!!
                val param = res.params[name]

                param?.let {
                    processParam(param, name, value)
                }
            }
            return res.handler.invoke(this)
        } catch (e: Exception) {
            e.printStackTrace()
            if (e is RuntimeException) {
                throw e
            } else {
                throw RuntimeException(e)
            }
        }
    }

    private fun processParam(param: Param, name: String, value: String) {
        try {
            val paramConverter = paramConverterProvider!!
                    .getConverter(param.type!!::class.java, null, null)
            if (paramConverter != null) {
                params.put(name, paramConverter.fromString(value))
            } else {
                when (param.type) {
                    Int::class -> params.put(name, value.toInt())
                    String::class -> params.put(name, value)
                    Double::class -> params.put(name, value.toDouble())
                    Float::class -> params.put(name, value.toFloat())
                    Boolean::class -> params.put(name, value.toBoolean())
                    Instant::class -> params.put(name, DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(value, Instant::from))
                    else -> {
                        throw WebApplicationException(Response.serverError()
                                .entity("Unknown paramter type: ${param.type!!.qualifiedName}")
                                .build())
                    }
                }
            }
        } catch (e: Exception) {
            throw WebApplicationException(Response.serverError()
                    .entity("${e::class.java.simpleName} ${e.localizedMessage}")
                    .build())
        }
    }
}