package com.steeplesoft.jerkey.server

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import kotlin.reflect.KClass

@JerkeyDslMarker
class Resource {
    var produces: String = MediaType.APPLICATION_JSON
    var consumes: String = MediaType.APPLICATION_JSON
    var path: String? = null
    var method: String = "get"
    var handler: ((CallContext) -> Response) = {
        Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Not implemented").build()
    }
    var params = mutableMapOf<String, Param>()
    var bodyType : KClass<*>? = null
    var sse : Boolean = false

    fun param(init: Param.() -> Unit) {
        val param = Param().apply(init)
        params.put(param.name!!, param)
    }

    override fun toString(): String {
        return "\n\t\tresource {\n\t\t\tproduces='$produces',\n" +
                "\t\t\tconsumes='$consumes',\n" +
                "\t\t\tpath=$path,\n" +
                "\t\t\tmethod='$method',\n" +
                "\t\t\thandler=$handler,\n" +
                "\t\t\tparams=$params,\n" +
                "\t\t\tbodyType=$bodyType,\n" +
                "\t\t\tsse=$sse }"
    }

}