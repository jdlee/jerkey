package com.steeplesoft.jerkey.server

import com.steeplesoft.jerkey.server.impl.JerkeyResourceConfig
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.UriBuilder

@JerkeyDslMarker
class Application {
    var produces: String = MediaType.APPLICATION_JSON
    var consumes: String = MediaType.WILDCARD
    var path = "/"
    var port = 8080
    var host = "localhost"
    val resources = mutableListOf<Resource>()

    fun resource(init: Resource.() -> Unit) {
        resources.add(Resource().apply(init))
    }

    fun start() {
        val rc = JerkeyResourceConfig(this)

        val baseUri = UriBuilder.fromUri("http://$host$path").port(port).build()
        val server = JdkHttpServerFactory.createHttpServer(baseUri, rc);
        Runtime.getRuntime().addShutdownHook(Thread(Runnable { server.stop(0) }))
        println("Listening on http://$host:$port$path. Press enter to quit.")
        readLine()
        server.stop(0)
    }

    override fun toString(): String {
        return "application {\n\tproduces='$produces',\n\tconsumes='$consumes',\n\tpath='$path',\n\tport=$port,\n\thost='$host',\n\tresources=$resources }"
    }
}


fun application(init: Application.() -> Unit): Application = Application().apply(init)
