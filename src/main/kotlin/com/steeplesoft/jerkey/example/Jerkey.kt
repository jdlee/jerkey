package com.steeplesoft.jerkey.example

import com.steeplesoft.jerkey.server.CallContext
import com.steeplesoft.jerkey.server.application
import org.glassfish.jersey.media.sse.EventOutput
import org.glassfish.jersey.media.sse.OutboundEvent
import org.glassfish.jersey.media.sse.SseFeature
import java.io.IOException
import java.time.Instant
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import kotlin.concurrent.thread

fun getItem(context: CallContext): Response {
    val id = context.params["id"] as Int?
    id?.let {
        val item = Item(id, "Item #$id")
        return Response.ok(item).build();
    }
    throw WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("No item specified").build())
}

fun listItems(context: CallContext): Response {
    val id = context.params["id"] as Int?
    val item = Item(id, "Item #$id")
    return Response.ok(item).build()
}

fun createItem(context: CallContext): Response {
    var item = context.body as Item
    return Response.ok("you created an item '${item.name}'!").build()
}

fun streamItems(context: CallContext): Response {
    val eventOutput = EventOutput()
    thread {
        try {
            for (i in 0..9) {
                val item = Item(i, "Item #${i + 1}")
                eventOutput.write(OutboundEvent.Builder()
                        .mediaType(MediaType.APPLICATION_JSON_TYPE)
                        .data(item)
                        .build())
            }
        } catch (e: IOException) {
            throw RuntimeException("Error when writing the event.", e)
        } finally {
            try {
                eventOutput.close()
            } catch (ioClose: IOException) {
                throw RuntimeException("Error when closing the event output.", ioClose)
            }
        }
    }

    return Response
            .status(Response.Status.OK)
            .entity(eventOutput)
            .type(SseFeature.SERVER_SENT_EVENTS_TYPE)
            .build()
}

fun main(args: Array<String>) {
    val app = application {
        port = 9090
        resource {
            path = "items"
            method = "get"
            handler = ::listItems
            param {
                source = "query"
                name = "id"
                type = Short::class
            }
            param {
                name = "modified"
                type = Instant::class
            }
        }
        resource {
            path = "items/{id}"
            method = "get"
            handler = ::getItem
            param {
                source = "path"
                name = "id"
                type = Int::class
            }
        }
        resource {
            path = "items"
            method = "post"
            handler = ::createItem
            bodyType = Item::class
            param {
                source = "body"
                name = "item"
                type = Item::class
            }
        }
        resource {
            path = "items/stream"
            method = "get"
            handler = ::streamItems
        }
    }

    println(app)

    app.start()
}