package com.steeplesoft.jerkey.example

data class Item (var id : Int? = null,
                 var name: String? = null) {
    constructor () : this(null, null)
}
